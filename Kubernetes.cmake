#
# External Kubernetes Client library
#

if(CMAKE_C_COMPILER_ID STREQUAL GNU AND CMAKE_C_COMPILER_VERSION VERSION_GREATER_EQUAL 13)
  set(GCC13_ARGS "-D CMAKE_C_FLAGS_INIT=-Wno-enum-int-mismatch")
endif()

# libwebsockets
ExternalProject_add(external-libwebsockets
  GIT_REPOSITORY https://github.com/warmcat/libwebsockets.git
  GIT_TAG v4.2-stable
  GIT_SHALLOW TRUE
  # CMAKE_COMMAND ${EXTERNAL_ENV} ${CMAKE_COMMAND} ${EXTERNAL_COMMON} -D LWS_WITHOUT_TESTAPPS=ON -D LWS_WITHOUT_TEST_SERVER=ON -D LWS_WITHOUT_TEST_SERVER_EXTPOLL=ON -D LWS_WITHOUT_TEST_PING=ON -D LWS_WITHOUT_TEST_CLIENT=ON
  CMAKE_ARGS ${EXTERNAL_COMMON} -D LWS_WITHOUT_TESTAPPS=ON -D LWS_WITHOUT_TEST_SERVER=ON -D LWS_WITHOUT_TEST_SERVER_EXTPOLL=ON -D LWS_WITHOUT_TEST_PING=ON -D LWS_WITHOUT_TEST_CLIENT=ON ${GCC13_ARGS}
)

add_library(websockets_shared IMPORTED SHARED GLOBAL)
set_target_properties(websockets_shared PROPERTIES
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwebsockets.so)
add_dependencies(websockets_shared external-libwebsockets)

# libyaml
ExternalProject_add(external-libyaml
  GIT_REPOSITORY https://github.com/yaml/libyaml
  GIT_TAG 0.2.5
  GIT_SHALLOW TRUE
  # CMAKE_COMMAND ${EXTERNAL_ENV} ${CMAKE_COMMAND} ${EXTERNAL_COMMON} -D BUILD_TESTING=OFF -D BUILD_SHARED_LIBS=ON
  CMAKE_ARGS ${EXTERNAL_COMMON} -D BUILD_TESTING=OFF -D BUILD_SHARED_LIBS=ON
)

add_library(libyaml IMPORTED SHARED GLOBAL)
set_target_properties(libyaml PROPERTIES
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libyaml.so)
add_dependencies(libyaml external-libyaml)

# libcurl
find_package(CURL 7.58 QUIET REQUIRED)

# kubernetes-client C lib
ExternalProject_add(external-kubernetes-client
  GIT_REPOSITORY https://github.com/kubernetes-client/c
  GIT_TAG v0.11.0
  GIT_SHALLOW TRUE
  SOURCE_SUBDIR kubernetes
  CMAKE_COMMAND env CMAKE_PREFIX_PATH=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}:${CMAKE_CURRENT_BINARY_DIR}/local ${CMAKE_COMMAND} ${EXTERNAL_COMMON}
  # PATCH_COMMAND patch -p1 -N < ${CMAKE_CURRENT_SOURCE_DIR}/kubernetes.patch
  DEPENDS external-libyaml external-libwebsockets ${KUBERNETES_DEPS}
)

add_library(kubernetes::kubernetes IMPORTED SHARED GLOBAL)
set_target_properties(kubernetes::kubernetes PROPERTIES
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libkubernetes.so)
add_dependencies(kubernetes::kubernetes external-kubernetes-client )

add_dependencies(tdaq-external-software
    external-libwebsockets
    external-libyaml
    external-kubernetes-client
  )
