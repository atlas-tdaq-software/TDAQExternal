# External packages for the ATLAS TDAQ software

Software that can be build with ExternalProject_Add()
can just be added to the top level CMakeLists.txt file.

More complicated packages should get their own cmake
file which is included. See IPBus.cmake as an example.

Python modules should be added to PythonPackages.cmake
Note that this file should just contain the package 
name an potential options. The version number should
go into requirements.txt which will be used as constraint
during the installation. This ensures a coherent set
of versions for all packages.

The result of running `pip freeze` on the final 
installation is stored in 

    $TDAQ_INST_PATH/external/$CMTCONFIG/lib/requirements.txt

