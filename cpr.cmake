ExternalProject_Add(external-cpr
  EXCLUDE_FROM_ALL TRUE
  GIT_REPOSITORY https://github.com/libcpr/cpr.git
  GIT_TAG 1.10.5
  GIT_SHALLOW TRUE
  CMAKE_COMMAND ${EXTERNAL_ENV} ${CMAKE_COMMAND} ${EXTERNAL_COMMON} -D CPR_USE_SYSTEM_CURL=ON
)

add_library(cpr::cpr SHARED IMPORTED GLOBAL)
target_link_libraries(cpr::cpr INTERFACE CURL::libcurl)
set_target_properties(cpr::cpr PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib64/libcpr.so)
add_dependencies(cpr::cpr external-cpr)
