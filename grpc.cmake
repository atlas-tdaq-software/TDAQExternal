# Google grpc
ExternalProject_add(external-grpc
  GIT_REPOSITORY    https://github.com/grpc/grpc
  GIT_TAG           v1.25.0
  BUILD_IN_SOURCE   1
  CONFIGURE_COMMAND git -C ../external-grpc submodule update -i
  # CONFIGURE COMMAND true
  BUILD_COMMAND     make -j ${NumProc}
  INSTALL_COMMAND   make prefix=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} install && make -C third_party/protobuf prefix=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} install
)

ExternalProject_add(external-grpc-java
  GIT_REPOSITORY  https://github.com/grpc/grpc-java.git
  GIT_TAG         v1.25.0
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND true
  BUILD_COMMAND cd compiler/src/java_plugin/cpp && ${CMAKE_CXX_COMPILER} -I ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/include -L ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib java_generator.cpp  java_generator.h  java_plugin.cpp
 -o grpc_java_plugin -lprotoc -l protobuf
  INSTALL_COMMAND cd  compiler/src/java_plugin/cpp && install grpc_java_plugin ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/bin
  DEPENDS external-grpc
)

ExternalProject_add(external-protobuf-python
  DOWNLOAD_COMMAND  true
  CONFIGURE_COMMAND true
  BUILD_COMMAND     true
  INSTALL_COMMAND   env PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} PYTHONPATH=${pip_python_path} ${PYTHON_EXECUTABLE} ${pip} install --user protobuf==3.11.1
)


ExternalProject_add(external-grpc-python
  DOWNLOAD_COMMAND  true
  CONFIGURE_COMMAND true
  BUILD_COMMAND     true
  INSTALL_COMMAND   env PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} PYTHONPATH=${pip_python_path} ${PYTHON_EXECUTABLE} ${pip} install --user grpcio==1.25.0
  DEPENDS external-grpc
)

add_dependencies(tdaq-external-software external-grpc external-grpc-java external-grpc-python)
