# Wt

ExternalProject_add(external-wt
  URL https://github.com/emweb/wt/archive/4.10.3.tar.gz
  CMAKE_COMMAND ${CMAKE_COMMAND} ${EXTERNAL_COMMON} -DBOOST_ROOT=${BOOST_ROOT} -DPNG_ROOT=${PNG_ROOT} -DZLIB_ROOT=${ZLIB_ROOT}
  DOWNLOAD_EXTRACT_TIMESTAMP TRUE
  )

add_library(wt IMPORTED SHARED GLOBAL)
set_target_properties(wt PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwt.so)
add_dependencies(wt external-wt)

add_library(wthttp IMPORTED SHARED GLOBAL)
set_target_properties(wthttp PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwthttp.so)
add_dependencies(wthttp external-wt)

add_library(wtdbo IMPORTED SHARED GLOBAL)
set_target_properties(wtdbo PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwtdbo.so)
add_dependencies(wtdbo external-wt)

add_library(wtdbosqlite3 IMPORTED SHARED GLOBAL)
set_target_properties(wtdbosqlite3 PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwtdbosqlite3.so)
add_dependencies(wtdbosqlite3 external-wt)

add_library(wttest IMPORTED SHARED GLOBAL)
set_target_properties(wttest PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libwttest.so)
add_dependencies(wttest external-wt)

add_dependencies(tdaq-external-software external-wt)
