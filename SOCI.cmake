
ExternalProject_add(
  external-soci
  URL https://github.com/SOCI/soci/archive/refs/tags/v4.0.3.tar.gz
  CMAKE_COMMAND env BOOST_ROOT=${BOOST_ROOT} MYSQL_DIR=${MYSQL_ROOT} ORACLE_HOME=${ORACLE_ROOT} env SQLITE_ROOT=${SQLITE_ROOT} ${CMAKE_COMMAND} ${EXTERNAL_COMMON}
  )
