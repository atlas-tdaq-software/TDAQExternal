#!/bin/bash
#
# lcgpy_pkg.sh <python> <pkg1> <pkg2>...
#
# Determine LCG packages needed for the additional Python
# packages installed.
# 
[ $# -lt 1 ] && exit 1

python=$1

shift

required=$(${python} -m pip show $(echo $@ | sed 's;external-;;g') | grep '^Requires: ' | sed -e 's/^Requires: //' | tr -d ',' | tr '\n' ' ' )

${python} -m pip show ${required} | egrep '^Location: |^Name: ' | fgrep --no-group-separator -B1 /cvmfs/sft | sed -e '/^Location:.*/d' -e 's/^Name: //' | sort -u | tr '\n' ' '
