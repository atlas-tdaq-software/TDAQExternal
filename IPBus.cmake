#
# External IPBus software
#

# pugixml
ExternalProject_add(external-pugixml
  EXCLUDE_FROM_ALL TRUE
  GIT_REPOSITORY https://github.com/zeux/pugixml.git
  GIT_TAG v1.14
  GIT_SHALLOW TRUE
  CMAKE_ARGS ${EXTERNAL_COMMON} -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_LIBDIR=lib
)

add_library(pugixml IMPORTED SHARED GLOBAL)
set_target_properties(pugixml PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libpugixml.so)
add_dependencies(pugixml external-pugixml)

find_package(Boost REQUIRED QUIET COMPONENTS thread filesystem regex chrono program_options unit_test_framework system)
find_package(Python REQUIRED QUIET COMPONENTS Development)

get_filename_component(boost_lib_dir ${Boost_INCLUDE_DIRS}/../lib ABSOLUTE)

get_filename_component(python_dir ${Python_EXECUTABLE} DIRECTORY)

# IPBus
ExternalProject_add(external-ipbus
  EXCLUDE_FROM_ALL TRUE
  GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-software/external/ipbus-software.git
  GIT_TAG v2.8.16
  GIT_SHALLOW TRUE
  # PATCH_COMMAND git --work-tree uhal/python/pybind11 --git-dir uhal/python/pybind11/.git checkout v2.10.4
  CONFIGURE_COMMAND true
  BUILD_IN_SOURCE 1
  BUILD_COMMAND env PYTHONHOME=${PYTHON_ROOT} PATH=${python_dir}:$ENV{PATH} PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} $(MAKE) EXTERN_BOOST_INCLUDE_PREFIX=${Boost_INCLUDE_DIRS} EXTERN_BOOST_LIB_PREFIX=${boost_lib_dir} EXTERN_PUGIXML_INCLUDE_PREFIX=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/include EXTERN_PUGIXML_LIB_PREFIX=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib Set=uhal BUILD_BOOST=0 BUILD_PUGIXML=0
  INSTALL_COMMAND env PATH=${python_dir}:$ENV{PATH} PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} $(MAKE) Set=uhal prefix=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} install
  DEPENDS external-pugixml
)

add_library(cactus_uhal_grammars IMPORTED SHARED GLOBAL)
set_target_properties(cactus_uhal_grammars PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libcactus_uhal_grammars.so)
add_dependencies(cactus_uhal_grammars external-ipbus)

add_library(cactus_uhal_log IMPORTED SHARED GLOBAL)
set_target_properties(cactus_uhal_log PROPERTIES 
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libcactus_uhal_log.so
  INTERFACE_LINK_LIBRARIES "Boost::thread")
add_dependencies(cactus_uhal_log external-ipbus)

add_library(cactus_uhal_uhal IMPORTED SHARED GLOBAL)
set_target_properties(cactus_uhal_uhal PROPERTIES 
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libcactus_uhal_uhal.so
  INTERFACE_LINK_LIBRARIES "cactus_uhal_grammars;cactus_uhal_log;pugixml;Boost::thread;Boost::filesystem;Boost::regex;Boost::chrono")
add_dependencies(cactus_uhal_uhal external-ipbus)

add_library(cactus_uhal_tests IMPORTED SHARED GLOBAL)
set_target_properties(cactus_uhal_tests PROPERTIES 
  IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib/libcactus_uhal_tests.so
  INTERFACE_LINK_LIBRARIES "cactus_uhal_uhal;Boost::thread;Boost::filesystem;Boost::program_options;Boost::unit_test_framework;Boost::system")
add_dependencies(cactus_uhal_tests external-ipbus)

add_dependencies(tdaq-external-software
  external-pugixml external-ipbus
  )


